﻿namespace Video_Downloader
{
    partial class optionsform2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialTabControl1 = new MaterialSkin.Controls.MaterialTabControl();
            this.Filetypetabpage = new System.Windows.Forms.TabPage();
            this.fiuletypegrobx = new System.Windows.Forms.GroupBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.materialTabSelector1 = new MaterialSkin.Controls.MaterialTabSelector();
            this.Closeoptionsbtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.downloadcheckbox = new MaterialSkin.Controls.MaterialCheckBox();
            this.downfailedcheckbox = new MaterialSkin.Controls.MaterialCheckBox();
            this.materialTabControl1.SuspendLayout();
            this.Filetypetabpage.SuspendLayout();
            this.fiuletypegrobx.SuspendLayout();
            this.SuspendLayout();
            // 
            // materialTabControl1
            // 
            this.materialTabControl1.Controls.Add(this.Filetypetabpage);
            this.materialTabControl1.Depth = 0;
            this.materialTabControl1.Location = new System.Drawing.Point(2, 115);
            this.materialTabControl1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControl1.Name = "materialTabControl1";
            this.materialTabControl1.SelectedIndex = 0;
            this.materialTabControl1.Size = new System.Drawing.Size(624, 211);
            this.materialTabControl1.TabIndex = 0;
            // 
            // Filetypetabpage
            // 
            this.Filetypetabpage.Controls.Add(this.fiuletypegrobx);
            this.Filetypetabpage.Location = new System.Drawing.Point(4, 22);
            this.Filetypetabpage.Name = "Filetypetabpage";
            this.Filetypetabpage.Padding = new System.Windows.Forms.Padding(3);
            this.Filetypetabpage.Size = new System.Drawing.Size(616, 185);
            this.Filetypetabpage.TabIndex = 1;
            this.Filetypetabpage.Text = "File type";
            this.Filetypetabpage.UseVisualStyleBackColor = true;
            // 
            // fiuletypegrobx
            // 
            this.fiuletypegrobx.BackColor = System.Drawing.Color.White;
            this.fiuletypegrobx.Controls.Add(this.listBox1);
            this.fiuletypegrobx.Location = new System.Drawing.Point(6, 6);
            this.fiuletypegrobx.Name = "fiuletypegrobx";
            this.fiuletypegrobx.Size = new System.Drawing.Size(604, 239);
            this.fiuletypegrobx.TabIndex = 0;
            this.fiuletypegrobx.TabStop = false;
            this.fiuletypegrobx.Text = "File type:";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "Mp4 Files"});
            this.listBox1.Location = new System.Drawing.Point(6, 19);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(591, 134);
            this.listBox1.TabIndex = 1;
            // 
            // materialTabSelector1
            // 
            this.materialTabSelector1.BaseTabControl = this.materialTabControl1;
            this.materialTabSelector1.Depth = 0;
            this.materialTabSelector1.Location = new System.Drawing.Point(0, 64);
            this.materialTabSelector1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabSelector1.Name = "materialTabSelector1";
            this.materialTabSelector1.Size = new System.Drawing.Size(634, 45);
            this.materialTabSelector1.TabIndex = 1;
            this.materialTabSelector1.Text = "materialTabSelector1";
            // 
            // Closeoptionsbtn
            // 
            this.Closeoptionsbtn.AutoSize = true;
            this.Closeoptionsbtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Closeoptionsbtn.Depth = 0;
            this.Closeoptionsbtn.Icon = null;
            this.Closeoptionsbtn.Location = new System.Drawing.Point(559, 331);
            this.Closeoptionsbtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Closeoptionsbtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.Closeoptionsbtn.Name = "Closeoptionsbtn";
            this.Closeoptionsbtn.Primary = false;
            this.Closeoptionsbtn.Size = new System.Drawing.Size(63, 36);
            this.Closeoptionsbtn.TabIndex = 2;
            this.Closeoptionsbtn.Text = "Close";
            this.Closeoptionsbtn.UseVisualStyleBackColor = true;
            this.Closeoptionsbtn.Click += new System.EventHandler(this.Closeoptionsbtn_Click);
            // 
            // downloadcheckbox
            // 
            this.downloadcheckbox.AutoSize = true;
            this.downloadcheckbox.Depth = 0;
            this.downloadcheckbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.downloadcheckbox.Location = new System.Drawing.Point(67, 64);
            this.downloadcheckbox.Margin = new System.Windows.Forms.Padding(0);
            this.downloadcheckbox.MouseLocation = new System.Drawing.Point(-1, -1);
            this.downloadcheckbox.MouseState = MaterialSkin.MouseState.HOVER;
            this.downloadcheckbox.Name = "downloadcheckbox";
            this.downloadcheckbox.Ripple = true;
            this.downloadcheckbox.Size = new System.Drawing.Size(152, 30);
            this.downloadcheckbox.TabIndex = 0;
            this.downloadcheckbox.Text = "Download complete";
            this.downloadcheckbox.UseVisualStyleBackColor = true;
            // 
            // downfailedcheckbox
            // 
            this.downfailedcheckbox.AutoSize = true;
            this.downfailedcheckbox.Depth = 0;
            this.downfailedcheckbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.downfailedcheckbox.Location = new System.Drawing.Point(291, 64);
            this.downfailedcheckbox.Margin = new System.Windows.Forms.Padding(0);
            this.downfailedcheckbox.MouseLocation = new System.Drawing.Point(-1, -1);
            this.downfailedcheckbox.MouseState = MaterialSkin.MouseState.HOVER;
            this.downfailedcheckbox.Name = "downfailedcheckbox";
            this.downfailedcheckbox.Ripple = true;
            this.downfailedcheckbox.Size = new System.Drawing.Size(129, 30);
            this.downfailedcheckbox.TabIndex = 1;
            this.downfailedcheckbox.Text = "Download failed";
            this.downfailedcheckbox.UseVisualStyleBackColor = true;
            // 
            // optionsform2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 374);
            this.Controls.Add(this.Closeoptionsbtn);
            this.Controls.Add(this.materialTabSelector1);
            this.Controls.Add(this.materialTabControl1);
            this.Name = "optionsform2";
            this.Text = "Options";
            this.Load += new System.EventHandler(this.Optionsform2_Load);
            this.materialTabControl1.ResumeLayout(false);
            this.Filetypetabpage.ResumeLayout(false);
            this.fiuletypegrobx.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialTabControl materialTabControl1;
        private System.Windows.Forms.TabPage Filetypetabpage;
        private MaterialSkin.Controls.MaterialTabSelector materialTabSelector1;
        private System.Windows.Forms.GroupBox fiuletypegrobx;
        private MaterialSkin.Controls.MaterialFlatButton Closeoptionsbtn;
        private System.Windows.Forms.ListBox listBox1;
        private MaterialSkin.Controls.MaterialCheckBox downloadcheckbox;
        private MaterialSkin.Controls.MaterialCheckBox downfailedcheckbox;
    }
}