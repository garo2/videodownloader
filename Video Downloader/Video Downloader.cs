﻿using MaterialSkin.Controls;
using System;
using MaterialSkin;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Video_Downloader
{
    public partial class Video_Downloader : MaterialForm

    {


        public static ListView listViewReference = null;

        public Video_Downloader()
        {
            InitializeComponent();
        }

        private void Video_Downloader_Load(object sender, EventArgs e)
        {
            listViewReference = listView1;
            string hstryTxtFile = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads\\Videos\\hisotryListFile.txt"; // Download the video in videos folder in "UserProfile" folder
            try
            {
                if (File.Exists(hstryTxtFile)) // Chechk if the HistryFile exist do the if statement. 
                {
                    string[] items = File.ReadAllLines(hstryTxtFile); //items array contains all readlines one-by-one 
                    foreach (string item in items)
                    {
                        string[] splitedItems = item.Split(','); // split the item which contains both the name of the video and it's size
                        ListViewItem lvi = new ListViewItem(splitedItems[0].ToString()); //add the Name of the video to the first column
                        lvi.SubItems.Add(splitedItems[1].ToString()); // then add the size of the video to the second column
                        
                        listView1.Items.Add(lvi); // add all the items to the HistoryTextFile

                    }
                }
            }
            catch
            { } // if the HistoryTextFile does not exist, catch the function.
            
            try
            {
                string videosDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads\\Videos\\"; // The path of the video downloading location
                Directory.CreateDirectory(videosDirectory);
            }
            catch
            {
                MessageBox.Show("Couldn't Create Videos Directory!", "", MessageBoxButtons.OK, MessageBoxIcon.Error); // show the fail in a MessageBox
            }
        }
        private void urlbutton_Click(object sender, EventArgs e)
        {
            FormURL formUrl = new FormURL();
            formUrl.Show(); // Open the AddUrlform
        }

        private void optionbtn_Click(object sender, EventArgs e)
        {
            optionsform2 formoption = new optionsform2();
            formoption.Show(); // Open the Optionsform
        }

        private void deletebtn_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (ListViewItem item in listView1.SelectedItems) // once you click on the delete button. You remove the selected Item
                {
                    item.Remove();
                }
            }
            catch { }
        }

        private void Helpbtn_Click(object sender, EventArgs e)
        {
            Helpwindow helpwind = new Helpwindow();
            helpwind.Show();// Open the Helpform
        }

        private void Closebtn_Click(object sender, EventArgs e)
        {
            ActiveForm.Close(); // Close the active form
        }
        
        private void Video_Downloader_FormClosed_1(object sender, FormClosedEventArgs e)
        {
            string hstryTxtFile = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads\\Videos\\hisotryListFile.txt";
            FileStream fStream = File.Open(hstryTxtFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite); // When You close the Video Downloader form, This code Create a hstryTxtFile if it doesn't exist 
                                                                                                                            // and add the all items in ListView1 to the hstryTxtFile
            TextWriter tw = new StreamWriter(fStream);
            StringBuilder strngBlder;
            if (listView1.Items.Count > 0) // If the list contains items, they will be added to the hstryTxtFile. 
            {
                foreach (ListViewItem lvi in listView1.Items)
                {
                    strngBlder = new StringBuilder();
                    foreach (ListViewItem.ListViewSubItem listViewSubItem in lvi.SubItems)
                    {
                        strngBlder.Append(string.Format("{0},", listViewSubItem.Text));
                    }
                    tw.WriteLine(strngBlder.ToString());
                }
        
            }
            tw.Flush();
            tw.Close();
        }
    }
}
