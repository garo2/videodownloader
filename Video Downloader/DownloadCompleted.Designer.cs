﻿namespace Video_Downloader
{
    partial class DownloadCompletedform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.filepathcompletedbtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.filepathdowncomptxt = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.Filebtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.namedowncompltxt = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.openfolderbtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.SuspendLayout();
            // 
            // filepathcompletedbtn
            // 
            this.filepathcompletedbtn.AutoSize = true;
            this.filepathcompletedbtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.filepathcompletedbtn.Depth = 0;
            this.filepathcompletedbtn.Icon = null;
            this.filepathcompletedbtn.Location = new System.Drawing.Point(15, 136);
            this.filepathcompletedbtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.filepathcompletedbtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.filepathcompletedbtn.Name = "filepathcompletedbtn";
            this.filepathcompletedbtn.Primary = false;
            this.filepathcompletedbtn.Size = new System.Drawing.Size(91, 36);
            this.filepathcompletedbtn.TabIndex = 0;
            this.filepathcompletedbtn.Text = "File path:";
            this.filepathcompletedbtn.UseVisualStyleBackColor = true;
            // 
            // filepathdowncomptxt
            // 
            this.filepathdowncomptxt.Depth = 0;
            this.filepathdowncomptxt.Enabled = false;
            this.filepathdowncomptxt.Hint = "";
            this.filepathdowncomptxt.Location = new System.Drawing.Point(113, 149);
            this.filepathdowncomptxt.MaxLength = 32767;
            this.filepathdowncomptxt.MouseState = MaterialSkin.MouseState.HOVER;
            this.filepathdowncomptxt.Name = "filepathdowncomptxt";
            this.filepathdowncomptxt.PasswordChar = '\0';
            this.filepathdowncomptxt.SelectedText = "";
            this.filepathdowncomptxt.SelectionLength = 0;
            this.filepathdowncomptxt.SelectionStart = 0;
            this.filepathdowncomptxt.Size = new System.Drawing.Size(443, 23);
            this.filepathdowncomptxt.TabIndex = 2;
            this.filepathdowncomptxt.TabStop = false;
            this.filepathdowncomptxt.UseSystemPasswordChar = false;
            // 
            // Filebtn
            // 
            this.Filebtn.AutoSize = true;
            this.Filebtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Filebtn.Depth = 0;
            this.Filebtn.Icon = null;
            this.Filebtn.Location = new System.Drawing.Point(15, 88);
            this.Filebtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Filebtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.Filebtn.Name = "Filebtn";
            this.Filebtn.Primary = false;
            this.Filebtn.Size = new System.Drawing.Size(63, 36);
            this.Filebtn.TabIndex = 3;
            this.Filebtn.Text = "Name:";
            this.Filebtn.UseVisualStyleBackColor = true;
            // 
            // namedowncompltxt
            // 
            this.namedowncompltxt.Depth = 0;
            this.namedowncompltxt.Enabled = false;
            this.namedowncompltxt.Hint = "";
            this.namedowncompltxt.Location = new System.Drawing.Point(113, 100);
            this.namedowncompltxt.MaxLength = 32767;
            this.namedowncompltxt.MouseState = MaterialSkin.MouseState.HOVER;
            this.namedowncompltxt.Name = "namedowncompltxt";
            this.namedowncompltxt.PasswordChar = '\0';
            this.namedowncompltxt.SelectedText = "";
            this.namedowncompltxt.SelectionLength = 0;
            this.namedowncompltxt.SelectionStart = 0;
            this.namedowncompltxt.Size = new System.Drawing.Size(443, 23);
            this.namedowncompltxt.TabIndex = 4;
            this.namedowncompltxt.TabStop = false;
            this.namedowncompltxt.UseSystemPasswordChar = false;
            // 
            // openfolderbtn
            // 
            this.openfolderbtn.AutoSize = true;
            this.openfolderbtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.openfolderbtn.Depth = 0;
            this.openfolderbtn.Icon = null;
            this.openfolderbtn.Location = new System.Drawing.Point(222, 200);
            this.openfolderbtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.openfolderbtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.openfolderbtn.Name = "openfolderbtn";
            this.openfolderbtn.Primary = false;
            this.openfolderbtn.Size = new System.Drawing.Size(110, 36);
            this.openfolderbtn.TabIndex = 6;
            this.openfolderbtn.Text = "Open folder";
            this.openfolderbtn.UseVisualStyleBackColor = true;
            this.openfolderbtn.Click += new System.EventHandler(this.openfolderbtn_Click);
            // 
            // DownloadCompletedform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 251);
            this.Controls.Add(this.openfolderbtn);
            this.Controls.Add(this.namedowncompltxt);
            this.Controls.Add(this.Filebtn);
            this.Controls.Add(this.filepathdowncomptxt);
            this.Controls.Add(this.filepathcompletedbtn);
            this.Name = "DownloadCompletedform";
            this.Text = " ";
            this.Load += new System.EventHandler(this.DownloadCompleted_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialFlatButton filepathcompletedbtn;
        private MaterialSkin.Controls.MaterialSingleLineTextField filepathdowncomptxt;
        private MaterialSkin.Controls.MaterialFlatButton Filebtn;
        private MaterialSkin.Controls.MaterialSingleLineTextField namedowncompltxt;
        private MaterialSkin.Controls.MaterialFlatButton openfolderbtn;
    }
}