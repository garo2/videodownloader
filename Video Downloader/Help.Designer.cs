﻿namespace Video_Downloader
{
    partial class Helpwindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detailshelpbtn = new MaterialSkin.Controls.MaterialLabel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Detailshelpbtn
            // 
            this.Detailshelpbtn.AutoSize = true;
            this.Detailshelpbtn.Depth = 0;
            this.Detailshelpbtn.Font = new System.Drawing.Font("Roboto", 11F);
            this.Detailshelpbtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Detailshelpbtn.Location = new System.Drawing.Point(20, 85);
            this.Detailshelpbtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.Detailshelpbtn.Name = "Detailshelpbtn";
            this.Detailshelpbtn.Size = new System.Drawing.Size(60, 19);
            this.Detailshelpbtn.TabIndex = 0;
            this.Detailshelpbtn.Text = "Details:";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Maiandra GD", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(24, 117);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(510, 245);
            this.textBox1.TabIndex = 1;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "This program will allow you to download videos only from Youtube. You will not be" +
    " able to download some of the video because they are encrypted, so try another o" +
    "ne";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Helpwindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 385);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Detailshelpbtn);
            this.Name = "Helpwindow";
            this.Text = "Help";
            this.Load += new System.EventHandler(this.Help_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel Detailshelpbtn;
        private System.Windows.Forms.TextBox textBox1;
    }
}