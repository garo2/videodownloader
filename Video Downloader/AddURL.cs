﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video_Downloader;
using System.IO;
using System.Threading;
using VideoLibrary;

namespace Video_Downloader
{
    public partial class FormURL : MaterialForm
    {
        private readonly MaterialSkinManager mSkinManager;
        private SaveFileDialog saveFileDialog1 = new SaveFileDialog();
        private string normalizedLink;

        public FormURL()
        {
            InitializeComponent();
            
            mSkinManager = MaterialSkinManager.Instance;
            mSkinManager.AddFormToManage(this); //use the mSkinManage as a skin for the form
            CheckForIllegalCrossThreadCalls = false; // to not block the other threads to change in the progress bar. 
            saveFileDialog1.Filter = "MP4 Files |*.mp4"; // download the videos with the filter mp4
        }     
        private void btnbrows1_Click(object sender, EventArgs e)
        {
            string videosDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads\\Videos\\"; //When you click on the browse button to choose a specific path to download the video
            saveFileDialog1.InitialDirectory = videosDirectory;
            
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtbxpathaddurl.Text = saveFileDialog1.FileName;
             }
        }
        private void downloadbtn_Click(object sender, EventArgs e)
        {
            YouTubeVideo videoUrl = null;
            YouTubeVideo audioUrl = null;

            try
            {

                var youTube = YouTube.Default; // starting point for YouTube actions
                IEnumerable<YouTubeVideo> videos = youTube.GetAllVideosAsync(normalizedLink).GetAwaiter().GetResult(); // gets a Video object with info about the video

                foreach (YouTubeVideo item in videos)
                {
                    if (item.AudioBitrate == -1 && item.Format == VideoFormat.Mp4 && item.Resolution == int.Parse(resolutionListbox.SelectedItem.ToString()))
                    {
                        videoUrl = item;
                    }
                    if (item.AdaptiveKind == AdaptiveKind.Audio && item.Resolution == -1 && item.AudioFormat == AudioFormat.Aac)
                    {
                        audioUrl = item;
                    }//get the AudioBitrate with the -1 and then catch the video with the VideoBitrate -1 
                }
            }
            catch
            {
                MessageBox.Show("You entered a wrong link.", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk); //Show an error message when the link is wrong
            }


            DownloadStatus frmDownloader = new DownloadStatus(videoUrl, audioUrl, txtbxpathaddurl.Text);
           try
            {
                frmDownloader.Show();

                this.Close();
                    //Open the DownloadStatus form to start the download
            }
            catch {
                MessageBox.Show("This video can not be downloaded, try another one!" +
                    "#1 Error occured, contact developer"); // If the informations are not eough to start the downoload, Show an error message. 
            }
        }

        private void FormURL_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false; // giving the permission to all thread to be able to change in the main function.
        }
        
        private void Resolutionlistbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (resolutionListbox.SelectedItem != null)//When the resolutionlistbox isnt empty, enable the download button.
            {
                downloadbtn.Enabled = true;
            }
        }
        
        

        void fetch_video_data()
        {
            try
            {
                if (resolutionListbox.Items.Count > 0) resolutionListbox.Items.Clear();

                var youtube = YouTube.Default;
           
                IEnumerable<YouTubeVideo> videos = youtube.GetAllVideosAsync(normalizedLink).GetAwaiter().GetResult();

                string videosDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads\\Videos\\";
                string videoPath = Utilities.SanitizeFilename(videos.ToArray()[0].Title) + ".mp4";

                txtbxpathaddurl.Text = videosDirectory + videoPath; 
                saveFileDialog1.FileName = videoPath;

                foreach (YouTubeVideo video in videos)
                {
                    if (video.Format == VideoFormat.Mp4 && video.AudioBitrate == -1)
                    {
                        resolutionListbox.Items.Add(video.Resolution);// getting the informations about the video's available resolutions and listing them in the resolutionlistbox

                    }
                }
            }
            catch (Exception ex)// used to debug the video-fetch-data code
            {
                //Debug.WriteLine(ex.Message);
            }
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                fetch_video_data();
            }
            catch
            {
                fetch_video_data();
            }
        }

        private void txtbxurl_TextChanged(object sender, EventArgs e)
        {
            if (txtbxurl.Text.ToLower().Contains("list"))
            {
                if (txtbxurl.Text.ToLower().Contains("youtube.com"))
                {
                    string idnolist = txtbxurl.Text.Split('&')[0]; // rewrite the link. If it has been taken from a playlist or anyvideo. Rewrite it the to the form which contains youtube.com without "list". By spliting the strings and add parts of them to the fixed link.
                    normalizedLink = idnolist.ToString();
                    backgroundWorker1.RunWorkerAsync();
                }
                else if (txtbxurl.Text.ToLower().Contains("youtu.be"))
                {
                    string idnolistyoutu = txtbxurl.Text.Split('/')[3];
                    string idnolistsplit = idnolistyoutu.Split('?')[0];
                    normalizedLink = "https://www.youtube.com/watch?v=" + idnolistsplit;
                    backgroundWorker1.RunWorkerAsync();
                }
            }

            else if (txtbxurl.Text.ToLower().Contains("youtube.com"))
            {
                normalizedLink = txtbxurl.Text;
                backgroundWorker1.RunWorkerAsync();
            }
            else if (txtbxurl.Text.ToLower().Contains("youtu.be"))
            {
                string id = txtbxurl.Text.Split('/')[3];
                normalizedLink = "https://www.youtube.com/watch?v=" + id.ToString();
                backgroundWorker1.RunWorkerAsync();
            }
            //// failed to decrypt the video (https://youtu.be/ktlQrO2Sifg), (https://youtu.be/K0MY8Jf48Ts?list=RDK0MY8Jf48Ts)
            // merge failed for many videos           
        }
    }
}
