﻿using System;
using System.Diagnostics;
using System.IO;

namespace Merge
{
    public class VAMerge
    {
        public static bool Merge(string videoPath, string audioPath, string outputPath)
        {
            string ffmpegPath = Environment.CurrentDirectory + @"\ffmpeg.exe";

            try
            {
                ProcessStartInfo psiFFMPEG = new ProcessStartInfo();
                psiFFMPEG.FileName = ffmpegPath;
                psiFFMPEG.Arguments = $"-i \"{videoPath}\" -i \"{audioPath}\" -c copy \"{outputPath}\"";

                try
                {
                    File.Delete(outputPath);

                }catch { }
                //Debug.WriteLine(psiFFMPEG.Arguments);
                psiFFMPEG.UseShellExecute = false;
                psiFFMPEG.CreateNoWindow = true;
                Process ffmpegProcess = new Process();
                ffmpegProcess.StartInfo = psiFFMPEG;
                ffmpegProcess.Start();
                ffmpegProcess.WaitForExit(-1);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}