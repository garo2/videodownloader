﻿namespace Video_Downloader
{
    partial class FormURL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resolutionListbox = new System.Windows.Forms.ComboBox();
            this.downloadbtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.txtbxpathaddurl = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.pathlbl = new MaterialSkin.Controls.MaterialLabel();
            this.btnbrows1 = new MaterialSkin.Controls.MaterialFlatButton();
            this.Resolutionlbl = new MaterialSkin.Controls.MaterialLabel();
            this.URLlbl = new MaterialSkin.Controls.MaterialLabel();
            this.txtbxurl = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // resolutionListbox
            // 
            this.resolutionListbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resolutionListbox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.resolutionListbox.FormattingEnabled = true;
            this.resolutionListbox.Location = new System.Drawing.Point(117, 168);
            this.resolutionListbox.Name = "resolutionListbox";
            this.resolutionListbox.Size = new System.Drawing.Size(121, 21);
            this.resolutionListbox.TabIndex = 3;
            this.resolutionListbox.SelectedIndexChanged += new System.EventHandler(this.Resolutionlistbox_SelectedIndexChanged);
            // 
            // downloadbtn
            // 
            this.downloadbtn.AutoSize = true;
            this.downloadbtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.downloadbtn.Depth = 0;
            this.downloadbtn.Enabled = false;
            this.downloadbtn.Icon = null;
            this.downloadbtn.Location = new System.Drawing.Point(415, 177);
            this.downloadbtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.downloadbtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.downloadbtn.Name = "downloadbtn";
            this.downloadbtn.Primary = false;
            this.downloadbtn.Size = new System.Drawing.Size(96, 36);
            this.downloadbtn.TabIndex = 11;
            this.downloadbtn.Text = "download";
            this.downloadbtn.UseVisualStyleBackColor = true;
            this.downloadbtn.Click += new System.EventHandler(this.downloadbtn_Click);
            // 
            // txtbxpathaddurl
            // 
            this.txtbxpathaddurl.Depth = 0;
            this.txtbxpathaddurl.Hint = "";
            this.txtbxpathaddurl.Location = new System.Drawing.Point(117, 129);
            this.txtbxpathaddurl.MaxLength = 32767;
            this.txtbxpathaddurl.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbxpathaddurl.Name = "txtbxpathaddurl";
            this.txtbxpathaddurl.PasswordChar = '\0';
            this.txtbxpathaddurl.SelectedText = "";
            this.txtbxpathaddurl.SelectionLength = 0;
            this.txtbxpathaddurl.SelectionStart = 0;
            this.txtbxpathaddurl.Size = new System.Drawing.Size(330, 23);
            this.txtbxpathaddurl.TabIndex = 13;
            this.txtbxpathaddurl.TabStop = false;
            this.txtbxpathaddurl.UseSystemPasswordChar = false;
            //this.txtbxpathaddurl.Click += new System.EventHandler(this.txtbxpathaddurl_Click);
            // 
            // pathlbl
            // 
            this.pathlbl.Depth = 0;
            this.pathlbl.Font = new System.Drawing.Font("Roboto", 11F);
            this.pathlbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pathlbl.Location = new System.Drawing.Point(12, 129);
            this.pathlbl.MouseState = MaterialSkin.MouseState.HOVER;
            this.pathlbl.Name = "pathlbl";
            this.pathlbl.Size = new System.Drawing.Size(89, 23);
            this.pathlbl.TabIndex = 14;
            this.pathlbl.Text = "Select Path:";
            // 
            // btnbrows1
            // 
            this.btnbrows1.AutoSize = true;
            this.btnbrows1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnbrows1.Depth = 0;
            this.btnbrows1.Icon = null;
            this.btnbrows1.Location = new System.Drawing.Point(467, 129);
            this.btnbrows1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnbrows1.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnbrows1.Name = "btnbrows1";
            this.btnbrows1.Primary = false;
            this.btnbrows1.Size = new System.Drawing.Size(32, 36);
            this.btnbrows1.TabIndex = 15;
            this.btnbrows1.Text = "...";
            this.btnbrows1.UseVisualStyleBackColor = true;
            this.btnbrows1.Click += new System.EventHandler(this.btnbrows1_Click);
            // 
            // Resolutionlbl
            // 
            this.Resolutionlbl.Depth = 0;
            this.Resolutionlbl.Font = new System.Drawing.Font("Roboto", 11F);
            this.Resolutionlbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Resolutionlbl.Location = new System.Drawing.Point(16, 167);
            this.Resolutionlbl.MouseState = MaterialSkin.MouseState.HOVER;
            this.Resolutionlbl.Name = "Resolutionlbl";
            this.Resolutionlbl.Size = new System.Drawing.Size(85, 21);
            this.Resolutionlbl.TabIndex = 17;
            this.Resolutionlbl.Text = "Resolution:";
            // 
            // URLlbl
            // 
            this.URLlbl.AutoSize = true;
            this.URLlbl.Depth = 0;
            this.URLlbl.Font = new System.Drawing.Font("Roboto", 11F);
            this.URLlbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.URLlbl.Location = new System.Drawing.Point(16, 88);
            this.URLlbl.MouseState = MaterialSkin.MouseState.HOVER;
            this.URLlbl.Name = "URLlbl";
            this.URLlbl.Size = new System.Drawing.Size(40, 19);
            this.URLlbl.TabIndex = 18;
            this.URLlbl.Text = "URL:";
            // 
            // txtbxurl
            // 
            this.txtbxurl.BackColor = System.Drawing.SystemColors.Control;
            this.txtbxurl.Depth = 0;
            this.txtbxurl.Hint = "";
            this.txtbxurl.Location = new System.Drawing.Point(117, 84);
            this.txtbxurl.MaxLength = 32767;
            this.txtbxurl.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbxurl.Name = "txtbxurl";
            this.txtbxurl.PasswordChar = '\0';
            this.txtbxurl.SelectedText = "";
            this.txtbxurl.SelectionLength = 0;
            this.txtbxurl.SelectionStart = 0;
            this.txtbxurl.Size = new System.Drawing.Size(369, 23);
            this.txtbxurl.TabIndex = 21;
            this.txtbxurl.TabStop = false;
            this.txtbxurl.UseSystemPasswordChar = false;
//            this.txtbxurl.Click += new System.EventHandler(this.materialSingleLineTextField1_Click);
            this.txtbxurl.TextChanged += new System.EventHandler(this.txtbxurl_TextChanged);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // FormURL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 229);
            this.Controls.Add(this.txtbxurl);
            this.Controls.Add(this.URLlbl);
            this.Controls.Add(this.Resolutionlbl);
            this.Controls.Add(this.btnbrows1);
            this.Controls.Add(this.pathlbl);
            this.Controls.Add(this.txtbxpathaddurl);
            this.Controls.Add(this.downloadbtn);
            this.Controls.Add(this.resolutionListbox);
            this.Name = "FormURL";
            this.Text = "Add URL";
            this.Load += new System.EventHandler(this.FormURL_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox resolutionListbox;
        private MaterialSkin.Controls.MaterialFlatButton downloadbtn;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbxpathaddurl;
        private MaterialSkin.Controls.MaterialLabel pathlbl;
        private MaterialSkin.Controls.MaterialFlatButton btnbrows1;
        private MaterialSkin.Controls.MaterialLabel Resolutionlbl;
        private MaterialSkin.Controls.MaterialLabel URLlbl;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbxurl;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}

