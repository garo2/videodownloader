﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using Merge;
using VideoLibrary;

namespace Video_Downloader
{
    public partial class DownloadStatus : MaterialForm
    {
        private YouTubeVideo videoToDownload;
        private YouTubeVideo audioToDownload;
        private string tempVideoPath;
        private string tempAudioPath;
        private string savePath;
        private bool videoFinished = false;
        private bool audioFinished = false;
        private double fileSize;
        private Thread videoDownloadThread;
        private Thread audioDownloadThread;
        private Thread mergeThread;
        private ListViewItem lvItem;
        private WebClient videoWebClient = new WebClient();
        private WebClient audioWebClient = new WebClient();

        private readonly MaterialSkinManager mSKinManager;




        public delegate void DownloadFormInvoker();


        DownloadFormInvoker downloadFormInvoker;

        private  void downloadFormLauncher()
        {
            DownloadCompletedform newDownloadComplete = new DownloadCompletedform(savePath);
            newDownloadComplete.Show();

        }



        public DownloadStatus(YouTubeVideo video, YouTubeVideo audio, string downloadPath)
        {
            InitializeComponent();

            downloadFormInvoker = (DownloadFormInvoker)downloadFormLauncher;

            mSKinManager = MaterialSkinManager.Instance;
            mSKinManager.AddFormToManage(this);
            this.videoToDownload = video;

            this.audioToDownload = audio;

            this.savePath = downloadPath;

            this.tempVideoPath = Path.GetTempFileName();
            this.tempAudioPath = Path.GetTempFileName();
            
            try
            {
                this.fileSize = Math.Round(((double)(GetFileSize(video.Uri) + GetFileSize(video.Uri))) / ((double)(1024 * 1024)), 1); // get the file size and divide it by (1048576) to get the size in MB. And then round it to 1 decimal. 
            }
            catch
            {
                this.Close();
            }
            this.mbcountsize.Text = fileSize.ToString();
        }
        private int GetFileSize(string link)
        {
            System.Net.WebRequest req = System.Net.HttpWebRequest.Create(link);
            req.Method = "HEAD";
            using (System.Net.WebResponse resp = req.GetResponse())
            {
                int ContentLength;
                if (int.TryParse(resp.Headers.Get("Content-Length"), out ContentLength))
                {
                    return (ContentLength );
                }// To get the filesize, we need to send webrequest to Youtube and that happens by wriitng (System.Net.HttpWebRequest.Create(link))
                //get the back the content-length 
            }
            return 0;
        }
        private void DownloadStatus_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            Cancelbtn.Enabled = false;
            Startbtn.Enabled = true;
        }
        public void VideoDownloadThreadMethod()
        {
            videoWebClient.DownloadProgressChanged += WebClient_VideoDownloadProgressChanged;
            videoWebClient.DownloadFileCompleted += WebClient_VideoDownloadFileCompleted;

            videoWebClient.DownloadFileTaskAsync(videoToDownload.Uri, tempVideoPath);
        }
        //Put the event for the download function for video. 

        private void WebClient_VideoDownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            videoFinished = true;
        }
        private void WebClient_VideoDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            double totalDownloaded = (((double)e.ProgressPercentage) / (double)100) * (double)fileSize;

            mbcountdownloaded.Text = Math.Round(totalDownloaded, 2).ToString();
            precentlbl.Text = Convert.ToInt32(e.ProgressPercentage).ToString() + "%";
            progressBar1.Value = Convert.ToInt32(e.ProgressPercentage);
        }// Start the download and shows the results in precentLabel and porgressBar1
        public void AudioDownloadThreadMethod()
        {
            audioWebClient.DownloadFileCompleted += WebClient_AudioDownloadFileCompleted;

            audioWebClient.DownloadFileTaskAsync(audioToDownload.Uri, tempAudioPath);
        }//Put the event for the download function for audio. 

        private void WebClient_AudioDownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            audioFinished = true;
        }

        public void MergeThreadMethod()
        {

            while((audioFinished && videoFinished) != true)
            {

                Thread.Sleep(500);
            }

            Thread.Sleep(250);
            // when the downloaded is not finished, let the thread to sleep 500ms, when it is, let it sleep 250ms
            Debug.WriteLine("Downloading of video and audio is done, merging files");
            if (VAMerge.Merge(tempVideoPath, tempAudioPath, savePath))
            {
             //   MessageBox.Show("Download complete!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

              
                this.Invoke(downloadFormInvoker);
                this.Close();
            }//when the downloaded is complete, show a messageBox that shows the download is complete. 
            else
            {
                MessageBox.Show("Merge failed :(", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } // If the merge failed, show a messageBox that shows the download failed. 
            Thread.Sleep(250);
            try
            {
                File.Delete(tempVideoPath);
                File.Delete(tempAudioPath);
            }
            catch
            {
            }
        }
        private void Startbtn_Click(object sender, EventArgs e)
        {
            lvItem = new ListViewItem(videoToDownload.Title);

            string lvItemstring = lvItem.ToString();
            string lvItemString = lvItem.ToString();
            lvItem.SubItems.Add(fileSize.ToString() + " MB");

            Video_Downloader.listViewReference.Items.Add(lvItem);
            // show the results in the labels add the item to the list box. 
            videoDownloadThread = new Thread(VideoDownloadThreadMethod);
            audioDownloadThread = new Thread(AudioDownloadThreadMethod);
            mergeThread = new Thread(MergeThreadMethod);
            videoDownloadThread.Start();
            audioDownloadThread.Start();
            mergeThread.Start();
            Cancelbtn.Enabled = true;
            Startbtn.Enabled = false;
        }
        

        private void Cancelbtn_Click(object sender, EventArgs e)
        {
            
        }

        private void Cancelbtn_Click_1(object sender, EventArgs e)
        {
            mergeThread.Abort();
            videoWebClient.CancelAsync();
            audioWebClient.CancelAsync();

            new Thread( () =>
              {
                Thread.Sleep(5000);
                File.Delete(tempVideoPath);
                File.Delete(tempAudioPath);
              }).Start();

            this.Close();
      //by clicking on the cancelButton you cancel the download, abort the merge and delete video path and audio path
        }
    }
}
