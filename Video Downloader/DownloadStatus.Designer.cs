﻿namespace Video_Downloader
{
    partial class DownloadStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DownloadStatusgrpbx = new System.Windows.Forms.GroupBox();
            this.precentlbl = new MaterialSkin.Controls.MaterialLabel();
            this.MBdownloaded = new MaterialSkin.Controls.MaterialLabel();
            this.mbcountdownloaded = new MaterialSkin.Controls.MaterialLabel();
            this.MBsize = new MaterialSkin.Controls.MaterialLabel();
            this.Downloadedfile = new MaterialSkin.Controls.MaterialLabel();
            this.mbcountsize = new MaterialSkin.Controls.MaterialLabel();
            this.filesize = new MaterialSkin.Controls.MaterialLabel();
            this.Startbtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.Cancelbtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.progressBar1 = new MaterialSkin.Controls.MaterialProgressBar();
            this.DownloadStatusgrpbx.SuspendLayout();
            this.SuspendLayout();
            // 
            // DownloadStatusgrpbx
            // 
            this.DownloadStatusgrpbx.Controls.Add(this.precentlbl);
            this.DownloadStatusgrpbx.Controls.Add(this.MBdownloaded);
            this.DownloadStatusgrpbx.Controls.Add(this.mbcountdownloaded);
            this.DownloadStatusgrpbx.Controls.Add(this.MBsize);
            this.DownloadStatusgrpbx.Controls.Add(this.Downloadedfile);
            this.DownloadStatusgrpbx.Controls.Add(this.mbcountsize);
            this.DownloadStatusgrpbx.Controls.Add(this.filesize);
            this.DownloadStatusgrpbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.DownloadStatusgrpbx.Location = new System.Drawing.Point(12, 78);
            this.DownloadStatusgrpbx.Name = "DownloadStatusgrpbx";
            this.DownloadStatusgrpbx.Size = new System.Drawing.Size(623, 121);
            this.DownloadStatusgrpbx.TabIndex = 0;
            this.DownloadStatusgrpbx.TabStop = false;
            this.DownloadStatusgrpbx.Text = "Download Status";
//            this.DownloadStatusgrpbx.Enter += new System.EventHandler(this.DownloadStatusgrpbx_Enter);
            // 
            // precentlbl
            // 
            this.precentlbl.AutoSize = true;
            this.precentlbl.Depth = 0;
            this.precentlbl.Font = new System.Drawing.Font("Roboto", 11F);
            this.precentlbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.precentlbl.Location = new System.Drawing.Point(313, 62);
            this.precentlbl.MouseState = MaterialSkin.MouseState.HOVER;
            this.precentlbl.Name = "precentlbl";
            this.precentlbl.Size = new System.Drawing.Size(28, 19);
            this.precentlbl.TabIndex = 18;
            this.precentlbl.Text = "0%";
            // 
            // MBdownloaded
            // 
            this.MBdownloaded.AutoSize = true;
            this.MBdownloaded.Depth = 0;
            this.MBdownloaded.Font = new System.Drawing.Font("Roboto", 11F);
            this.MBdownloaded.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MBdownloaded.Location = new System.Drawing.Point(267, 62);
            this.MBdownloaded.MouseState = MaterialSkin.MouseState.HOVER;
            this.MBdownloaded.Name = "MBdownloaded";
            this.MBdownloaded.Size = new System.Drawing.Size(30, 19);
            this.MBdownloaded.TabIndex = 5;
            this.MBdownloaded.Text = "Mb";
            // 
            // mbcountdownloaded
            // 
            this.mbcountdownloaded.AutoSize = true;
            this.mbcountdownloaded.Depth = 0;
            this.mbcountdownloaded.Font = new System.Drawing.Font("Roboto", 11F);
            this.mbcountdownloaded.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mbcountdownloaded.Location = new System.Drawing.Point(217, 62);
            this.mbcountdownloaded.MouseState = MaterialSkin.MouseState.HOVER;
            this.mbcountdownloaded.Name = "mbcountdownloaded";
            this.mbcountdownloaded.Size = new System.Drawing.Size(13, 19);
            this.mbcountdownloaded.TabIndex = 4;
            this.mbcountdownloaded.Text = ".";
            // 
            // MBsize
            // 
            this.MBsize.AutoSize = true;
            this.MBsize.Depth = 0;
            this.MBsize.Font = new System.Drawing.Font("Roboto", 11F);
            this.MBsize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MBsize.Location = new System.Drawing.Point(267, 31);
            this.MBsize.MouseState = MaterialSkin.MouseState.HOVER;
            this.MBsize.Name = "MBsize";
            this.MBsize.Size = new System.Drawing.Size(30, 19);
            this.MBsize.TabIndex = 3;
            this.MBsize.Text = "Mb";
            // 
            // Downloadedfile
            // 
            this.Downloadedfile.AutoSize = true;
            this.Downloadedfile.Depth = 0;
            this.Downloadedfile.Font = new System.Drawing.Font("Roboto", 11F);
            this.Downloadedfile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Downloadedfile.Location = new System.Drawing.Point(6, 62);
            this.Downloadedfile.MouseState = MaterialSkin.MouseState.HOVER;
            this.Downloadedfile.Name = "Downloadedfile";
            this.Downloadedfile.Size = new System.Drawing.Size(96, 19);
            this.Downloadedfile.TabIndex = 2;
            this.Downloadedfile.Text = "Downloaded:";
            // 
            // mbcountsize
            // 
            this.mbcountsize.AutoSize = true;
            this.mbcountsize.Depth = 0;
            this.mbcountsize.Font = new System.Drawing.Font("Roboto", 11F);
            this.mbcountsize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mbcountsize.Location = new System.Drawing.Point(217, 30);
            this.mbcountsize.MouseState = MaterialSkin.MouseState.HOVER;
            this.mbcountsize.Name = "mbcountsize";
            this.mbcountsize.Size = new System.Drawing.Size(13, 19);
            this.mbcountsize.TabIndex = 1;
            this.mbcountsize.Text = ".";
            // 
            // filesize
            // 
            this.filesize.AutoSize = true;
            this.filesize.Depth = 0;
            this.filesize.Font = new System.Drawing.Font("Roboto", 11F);
            this.filesize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.filesize.Location = new System.Drawing.Point(6, 31);
            this.filesize.MouseState = MaterialSkin.MouseState.HOVER;
            this.filesize.Name = "filesize";
            this.filesize.Size = new System.Drawing.Size(64, 19);
            this.filesize.TabIndex = 0;
            this.filesize.Text = "File size";
            // 
            // Startbtn
            // 
            this.Startbtn.AutoSize = true;
            this.Startbtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Startbtn.Depth = 0;
            this.Startbtn.Icon = null;
            this.Startbtn.Location = new System.Drawing.Point(257, 234);
            this.Startbtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Startbtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.Startbtn.Name = "Startbtn";
            this.Startbtn.Primary = false;
            this.Startbtn.Size = new System.Drawing.Size(64, 36);
            this.Startbtn.TabIndex = 1;
            this.Startbtn.Text = "Start";
            this.Startbtn.UseVisualStyleBackColor = true;
            this.Startbtn.Click += new System.EventHandler(this.Startbtn_Click);
            // 
            // Cancelbtn
            // 
            this.Cancelbtn.AutoSize = true;
            this.Cancelbtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Cancelbtn.Depth = 0;
            this.Cancelbtn.Icon = null;
            this.Cancelbtn.Location = new System.Drawing.Point(329, 234);
            this.Cancelbtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Cancelbtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.Cancelbtn.Name = "Cancelbtn";
            this.Cancelbtn.Primary = false;
            this.Cancelbtn.Size = new System.Drawing.Size(73, 36);
            this.Cancelbtn.TabIndex = 2;
            this.Cancelbtn.Text = "Cancel";
            this.Cancelbtn.UseVisualStyleBackColor = true;
            this.Cancelbtn.Click += new System.EventHandler(this.Cancelbtn_Click_1);
            // 
            // progressBar1
            // 
            this.progressBar1.Depth = 0;
            this.progressBar1.Location = new System.Drawing.Point(12, 220);
            this.progressBar1.MouseState = MaterialSkin.MouseState.HOVER;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(623, 5);
            this.progressBar1.TabIndex = 17;
 //           this.progressBar1.Click += new System.EventHandler(this.materialProgressBar1_Click);
            // 
            // DownloadStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 289);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.Cancelbtn);
            this.Controls.Add(this.Startbtn);
            this.Controls.Add(this.DownloadStatusgrpbx);
            this.Name = "DownloadStatus";
            this.Text = "Download Status";
            this.Load += new System.EventHandler(this.DownloadStatus_Load);
            this.DownloadStatusgrpbx.ResumeLayout(false);
            this.DownloadStatusgrpbx.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox DownloadStatusgrpbx;
        private MaterialSkin.Controls.MaterialLabel mbcountsize;
        private MaterialSkin.Controls.MaterialLabel filesize;
        private MaterialSkin.Controls.MaterialLabel MBdownloaded;
        private MaterialSkin.Controls.MaterialLabel mbcountdownloaded;
        private MaterialSkin.Controls.MaterialLabel MBsize;
        private MaterialSkin.Controls.MaterialLabel Downloadedfile;
        private MaterialSkin.Controls.MaterialFlatButton Startbtn;
        private MaterialSkin.Controls.MaterialFlatButton Cancelbtn;
        private MaterialSkin.Controls.MaterialLabel precentlbl;
        private MaterialSkin.Controls.MaterialProgressBar progressBar1;
    }
}