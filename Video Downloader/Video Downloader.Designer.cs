﻿namespace Video_Downloader
{
    partial class Video_Downloader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.urlbutton = new MaterialSkin.Controls.MaterialFlatButton();
            this.listView1 = new System.Windows.Forms.ListView();
            this.listviewName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sizeColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.optionbtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.deletebtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.Helpbtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.Closebtn = new MaterialSkin.Controls.MaterialFlatButton();
            this.SuspendLayout();
            // 
            // urlbutton
            // 
            this.urlbutton.AutoSize = true;
            this.urlbutton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.urlbutton.BackgroundImage = global::Video_Downloader.Properties.Resources.Untitled_1;
            this.urlbutton.Depth = 0;
            this.urlbutton.Icon = null;
            this.urlbutton.Location = new System.Drawing.Point(12, 70);
            this.urlbutton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.urlbutton.MouseState = MaterialSkin.MouseState.HOVER;
            this.urlbutton.Name = "urlbutton";
            this.urlbutton.Primary = false;
            this.urlbutton.Size = new System.Drawing.Size(76, 36);
            this.urlbutton.TabIndex = 7;
            this.urlbutton.Text = "Add URL";
            this.urlbutton.UseVisualStyleBackColor = true;
            this.urlbutton.Click += new System.EventHandler(this.urlbutton_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.listviewName,
            this.sizeColumn});
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(12, 115);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(725, 489);
            this.listView1.TabIndex = 8;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // listviewName
            // 
            this.listviewName.Text = "Name";
            this.listviewName.Width = 366;
            // 
            // sizeColumn
            // 
            this.sizeColumn.Text = "Size";
            this.sizeColumn.Width = 167;
            // 
            // optionbtn
            // 
            this.optionbtn.AutoSize = true;
            this.optionbtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.optionbtn.Depth = 0;
            this.optionbtn.Icon = null;
            this.optionbtn.Location = new System.Drawing.Point(173, 70);
            this.optionbtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.optionbtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.optionbtn.Name = "optionbtn";
            this.optionbtn.Primary = false;
            this.optionbtn.Size = new System.Drawing.Size(79, 36);
            this.optionbtn.TabIndex = 9;
            this.optionbtn.Text = "Options";
            this.optionbtn.UseVisualStyleBackColor = true;
            this.optionbtn.Click += new System.EventHandler(this.optionbtn_Click);
            // 
            // deletebtn
            // 
            this.deletebtn.AutoSize = true;
            this.deletebtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deletebtn.Depth = 0;
            this.deletebtn.Icon = null;
            this.deletebtn.Location = new System.Drawing.Point(96, 70);
            this.deletebtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.deletebtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.deletebtn.Name = "deletebtn";
            this.deletebtn.Primary = false;
            this.deletebtn.Size = new System.Drawing.Size(69, 36);
            this.deletebtn.TabIndex = 10;
            this.deletebtn.Text = "delete";
            this.deletebtn.UseVisualStyleBackColor = true;
            this.deletebtn.Click += new System.EventHandler(this.deletebtn_Click);
            // 
            // Helpbtn
            // 
            this.Helpbtn.AutoSize = true;
            this.Helpbtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Helpbtn.Depth = 0;
            this.Helpbtn.Icon = null;
            this.Helpbtn.Location = new System.Drawing.Point(260, 70);
            this.Helpbtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Helpbtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.Helpbtn.Name = "Helpbtn";
            this.Helpbtn.Primary = false;
            this.Helpbtn.Size = new System.Drawing.Size(55, 36);
            this.Helpbtn.TabIndex = 11;
            this.Helpbtn.Text = "Help";
            this.Helpbtn.UseVisualStyleBackColor = true;
            this.Helpbtn.Click += new System.EventHandler(this.Helpbtn_Click);
            // 
            // Closebtn
            // 
            this.Closebtn.AutoSize = true;
            this.Closebtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Closebtn.Depth = 0;
            this.Closebtn.Icon = null;
            this.Closebtn.Location = new System.Drawing.Point(673, 613);
            this.Closebtn.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Closebtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.Closebtn.Name = "Closebtn";
            this.Closebtn.Primary = false;
            this.Closebtn.Size = new System.Drawing.Size(63, 36);
            this.Closebtn.TabIndex = 12;
            this.Closebtn.Text = "Close";
            this.Closebtn.UseVisualStyleBackColor = true;
            this.Closebtn.Click += new System.EventHandler(this.Closebtn_Click);
            // 
            // Video_Downloader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(746, 657);
            this.Controls.Add(this.Closebtn);
            this.Controls.Add(this.Helpbtn);
            this.Controls.Add(this.deletebtn);
            this.Controls.Add(this.optionbtn);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.urlbutton);
            this.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Name = "Video_Downloader";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Video Downloader";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Video_Downloader_FormClosed_1);
            this.Load += new System.EventHandler(this.Video_Downloader_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialFlatButton urlbutton;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader listviewName;
        private MaterialSkin.Controls.MaterialFlatButton optionbtn;
        private MaterialSkin.Controls.MaterialFlatButton deletebtn;
        private MaterialSkin.Controls.MaterialFlatButton Helpbtn;
        private MaterialSkin.Controls.MaterialFlatButton Closebtn;
        private System.Windows.Forms.ColumnHeader sizeColumn;
    }
}