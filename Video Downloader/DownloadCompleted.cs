﻿using MaterialSkin.Controls;
using System;
using MaterialSkin;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace Video_Downloader
{
    public partial class DownloadCompletedform : MaterialForm
    {
        private readonly MaterialSkinManager mSkinManager;
        private string FilePath;

        public DownloadCompletedform(string FilePath)
        {
            InitializeComponent();
            mSkinManager = MaterialSkinManager.Instance;
            mSkinManager.AddFormToManage(this);

            this.FilePath = FilePath;

        }
        
        private void DownloadCompleted_Load(object sender, EventArgs e)
        {
            
            
     
            filepathdowncomptxt.Text =  FilePath;


            namedowncompltxt.Text = Path.GetFileName(FilePath);
        }
        // Show the name of the video and it's path
        
        private void openfolderbtn_Click(object sender, EventArgs e)
        {
            string folderPath = FilePath.Replace(Path.GetFileName(FilePath), "");

            Process.Start("explorer", folderPath); // open the folder path where the video has been downloaded
        }
    }
}
